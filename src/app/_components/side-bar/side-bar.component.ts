import { Component, OnInit } from '@angular/core';
import { MediaService } from '../../_services/media.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  currentTab;
  constructor(private mediaService: MediaService) { }

  ngOnInit() {
  }

  tabClick(tabName: string): void {
    this.currentTab = tabName;
    this.mediaService.filter.type = tabName;
    this.mediaService.filterView();
  }

}
