import { Component, OnInit } from '@angular/core';
import { MediaService } from '../../_services/media.service';
import { Media } from '../../_models';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css']
})
export class TableViewComponent implements OnInit {
  constructor(private mediaService: MediaService) { }

  ngOnInit() {
  }

}
