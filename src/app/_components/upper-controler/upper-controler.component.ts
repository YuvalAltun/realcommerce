import { Component, OnInit } from '@angular/core';
import { MediaService } from '../../_services/media.service';

@Component({
  selector: 'app-upper-controler',
  templateUrl: './upper-controler.component.html',
  styleUrls: ['./upper-controler.component.css']
})
export class UpperControlerComponent implements OnInit {
  searchWord = '';
  constructor(public mediaService: MediaService) { }

  ngOnInit() {
  }

  clear() {
    this.searchWord = '';
    this.mediaService.filter.searchWord = '';
    this.mediaService.filterView();
  }
  refresh() {
    this.mediaService.refreshData();
  }
  badRefresh() {
    this.mediaService.refreshDataWithError();
  }
  search(searchWord: string) {
    this.mediaService.filter.searchWord = this.searchWord;
    this.mediaService.filterView();
  }
  sort() {
    this.mediaService.sort();
  }

}
