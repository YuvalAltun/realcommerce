import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpperControlerComponent } from './upper-controler.component';

describe('UpperControlerComponent', () => {
  let component: UpperControlerComponent;
  let fixture: ComponentFixture<UpperControlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpperControlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpperControlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
