import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MediaService } from './../../_services/media.service';
import { Media } from '../../_models/media';
@Component({
  selector: 'app-media-view',
  templateUrl: './media-view.component.html',
  styleUrls: ['./media-view.component.css']
})
export class MediaViewComponent implements OnInit {
  media: Media;
  constructor(
    private route: ActivatedRoute,
    private mediaService: MediaService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getMedia();
  }

  getMedia(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.mediaService.getMedia(id)
      .subscribe(media => this.media = media);
  }

  goBack(): void {
    this.location.back();
  }
}
