import { Component, OnInit } from '@angular/core';
import { Media } from '../../_models';
import { MediaService } from '../../_services/media.service';

@Component({
  selector: 'app-frame-view',
  templateUrl: './frame-view.component.html',
  styleUrls: ['./frame-view.component.css']
})
export class FrameViewComponent implements OnInit {
  medias: Media[];
  constructor(private mediaService: MediaService) { }

  ngOnInit() {
    this.medias = this.mediaService.getMedias();
  }

}
