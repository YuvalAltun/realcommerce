import { Media } from './media';

export class MediaResponse {
    results: Media[];
}
