export class SideBarType {
    type: string;
    amount: number;
    constructor(amount: number, type: string) {
        this.amount = amount;
        this.type = type;
    }
}
