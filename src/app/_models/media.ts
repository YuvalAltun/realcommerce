export class Media {
    Title: string;
    Year: number;
    imdbID: string;
    Type: string;
    Poster: string;
  }
