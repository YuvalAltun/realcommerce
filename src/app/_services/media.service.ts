import { Injectable } from '@angular/core';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError, of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Media, MediaResponse, SideBarType } from './../_models';

import { MessageService } from './message.service';
@Injectable({
  providedIn: 'root'
})
export class MediaService {
  medias: Media[];
  mediaByType: any;
  currentView: Media[];
  jsonURL = './assets/response.json';
  badJsonURL = './assets/respons.json';
  sortDirection = true;
  filter: {type: string, searchWord: string} = {type: '', searchWord: '' };

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService) {
      this.refreshData();
     }


getJsonData(path: string): Observable<Response> {
  return this.httpClient.get<Response>(path);
}

refreshData() {
  this.clearData();
  this.getJsonData(this.jsonURL).subscribe(
    (mediaResponse) => {
      this.medias = mediaResponse['results'];
      this.setMediaByType();
      this.currentView = this.medias; },
    (error) =>  this.messageService.add('could not fetch data from server')
  );
}

refreshDataWithError() {
  this.clearData();
  this.getJsonData(this.badJsonURL).subscribe(
    (mediaResponse) => {
      this.medias = mediaResponse['results'];
      this.setMediaByType();
      this.currentView = this.medias; },
    (error) =>  this.messageService.add('could not fetch data from server')
  );
}

clearData() {
  this.medias = null;
  this.mediaByType = null;
  this.currentView = null;
}

setMediaByType() {
  this.medias ? this.mediaByType = this.medias.reduce(
    (accum, media) => {
      const type = media.Type;
       accum[type] != null  ? accum[type].push(media) : accum[type] = [media];
       return accum;
  }, {}) : this.messageService.add('there are no media to display');
}
getTypesForSideBar(): SideBarType[] {
  const types: SideBarType[] = [];
  Object.keys(this.mediaByType).forEach((key) => types.push(new SideBarType(this.mediaByType[key].length, key)));
  return types;
}

getMedias() {
  return this.currentView;
}
getMedia(id: string): Observable<Media> {
  return of(this.medias.find((media => media.imdbID === id)));
}

filterView() {
  this.currentView = this.medias.filter((media) => (this.filter.type === '' || media.Type === this.filter.type) &&
   (this.filter.searchWord === '' || media.Title.toLocaleLowerCase().includes(this.filter.searchWord.toLocaleLowerCase())));
}
sort() {
  this.sortDirection ? this.currentView = this.currentView.sort((one, two) => (one.Title > two.Title ? -1 : 1)) :
  this.currentView = this.currentView.sort((one, two) => (one.Title > two.Title ? 1 : -1));
  this.sortDirection = !this.sortDirection;
}


private handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    console.error('An error occurred:', error.error.message);
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
  }
  // return an observable with a user-facing error message
  return throwError(
    'Something bad happened; please try again later.');
}
}
