import { Component } from '@angular/core';
import { MediaService } from './_services/media.service';
import { MessageService } from './_services/message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rc-ha';
  view: keyof ToggleState  = 'frame';
  constructor(private mediaService: MediaService,
     private messageService: MessageService) {
      console.log('app component constructor');

  }

  toggleView() {
    this.view === 'frame' ? this.view = 'table' : this.view = 'frame';
  }

}
interface ToggleState {
  table;
  frame;
}
