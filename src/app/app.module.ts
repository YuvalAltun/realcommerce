import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FrameViewComponent } from './_components/frame-view/frame-view.component';
import { TableViewComponent } from './_components/table-view/table-view.component';
import { MediaCardComponent } from './_components/media-card/media-card.component';
import { UpperControlerComponent } from './_components/upper-controler/upper-controler.component';
import { SideBarComponent } from './_components/side-bar/side-bar.component';
import { MessageComponent } from './_components/message/message.component';
import { MediaViewComponent } from './_components/media-view/media-view.component';

@NgModule({
  declarations: [
    AppComponent,
    FrameViewComponent,
    TableViewComponent,
    MediaCardComponent,
    UpperControlerComponent,
    SideBarComponent,
    MessageComponent,
    MediaViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
