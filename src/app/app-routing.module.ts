import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrameViewComponent } from './_components/frame-view/frame-view.component';
import { MediaViewComponent } from './_components/media-view/media-view.component';
import { TableViewComponent } from './_components/table-view/table-view.component';

const routes: Routes = [
  {path: '', redirectTo: 'frame', pathMatch: 'full'},
  {path: 'frame', component: FrameViewComponent },
  {path: 'table', component: TableViewComponent },
  {path: 'mediaView/:id', component: MediaViewComponent },
  {path: '**', redirectTo: 'frame' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
